import React from "react";
import Header from "../../components/Header";

const homepage = ({ title }) => {
  const myList = [
    "Make Coffee",
    "Log on",
    "Watch Video",
    "Drink Coffee",
    "Become Good Programmer",
    "Do donuts",
  ];

  return (
    <div>
      <h1>This is my to do list:</h1>
      <ul>
        {myList.map((el, index) => {
          return <Header key={index} name={el} />;
        })}
      </ul>
    </div>
  );
};

export default homepage;
